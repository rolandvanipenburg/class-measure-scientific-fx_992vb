# -*- cperl; cperl-indent-level: 4 -*-
## no critic (RequireExplicitPackage RequireEndWithOne ProhibitMagicNumbers)
use 5.016;
use strict;
use warnings;
use utf8;

use Test::More;
my $test_warnings = $ENV{'AUTHOR_TESTING'} && eval { require Test::NoWarnings };

use version;

our $VERSION = v0.0.7;
Test::More::plan 'tests' => 11;

sub iz {
    my ( $got, $exp, $msg ) = @_;
    my $FMT = q{%.15g};
    return Test::More::is( sprintf( $FMT, $got ), sprintf( $FMT, $exp ), $msg );
}

require Class::Measure::Scientific::FX_992vb;
my $m = Class::Measure::Scientific::FX_992vb->pressure( 1, 'pa' );
iz( $m->pa(), 1, q{1 Pascal via pressure} );
iz( $m->at(), 1.019_716_212_977_93e-05,
    q{1 Pascal in technical atmosphere via pressure} );
iz( $m->atm(), 9.869_232_667_160_13e-06,
    q{1 Pascal in standard atmosphere via pressure} );
iz( $m->mH2O(), 0.000_101_971_621_297_793,
    q{1 Pascal in meters of water via pressure} );
iz( $m->mmHg(), 0.007_500_616_827_041_7,
    q{1 Pascal in millimeters of mercury via pressure} );
iz( $m->Torr(), 0.007_500_616_827_041_7, q{1 Pascal in Torr via pressure} );
iz( $m->ftH2O(), 0.000_334_552_563_312_969,
    q{1 Pascal in foot of water via pressure} );
iz( $m->inH2O(), 0.004_014_630_759_755_62,
    q{1 Pascal in inch of water via pressure} );
iz( $m->inHg(), 0.000_295_299_875_080_795,
    q{1 Pascal in inch of mercury via pressure} );
iz( $m->psi(), 0.000_145_037_737_730_175, q{1 Pascal in PSI via pressure} );

## no critic (RequireInterpolationOfMetachars)
my $msg = q{Author test. Install Test::NoWarnings and set }
  . q{$ENV{AUTHOR_TESTING} to a true value to run.};
SKIP: {
    if ( !$test_warnings ) {
        Test::More::skip $msg, 1;
    }
}
$test_warnings && Test::NoWarnings::had_no_warnings();
