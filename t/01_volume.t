# -*- cperl; cperl-indent-level: 4 -*-
## no critic (RequireExplicitPackage RequireEndWithOne ProhibitMagicNumbers)
use 5.016;
use strict;
use warnings;
use utf8;

use Test::More;
my $test_warnings = $ENV{'AUTHOR_TESTING'} && eval { require Test::NoWarnings };

use version;

our $VERSION = v0.0.7;
Test::More::plan 'tests' => 35;

sub iz {
    my ( $got, $exp, $msg ) = @_;
    my $FMT = q{%.15g};
    return Test::More::is( sprintf( $FMT, $got ), sprintf( $FMT, $exp ), $msg );
}

require Class::Measure::Scientific::FX_992vb;
my $m = Class::Measure::Scientific::FX_992vb->volume( 1, 'm3' );
iz( $m->m3(),         1,                    q{1m3 via volume} );
iz( $m->bbl(),        6.289_810_770_748_6,  q{1m3 in barrel via volume} );
iz( $m->dry_barrel(), 8.648_489_809_643_33, q{1m3 in dry barrel via volume} );
iz( $m->bushel_uk(),  27.496_099_826_031_2, q{1m3 in bushel uk via volume} );
iz( $m->bu(),         28.377_593_263_942_1, q{1m3 in bushel via volume} );
iz( $m->cu(),         4_226.752_837_730_37, q{1m3 in cu via volume} );
iz( $m->pt_dry(),     1_816.165_968_536_06, q{1m3 in dry pint via volume} );
iz( $m->fbm(),        423.776_000_657_863,  q{1m3 in board foot via volume} );
iz( $m->floz_uk(), 35_195.007_768_571_7, q{1m3 in fluid ounce UK via volume} );
iz( $m->floz_us(), 33_814.022_701_843,   q{1m3 in fluid ounce US via volume} );
iz( $m->gal_uk(),  219.968_798_553_573,  q{1m3 in gallon UK via volume} );
iz( $m->gal_us(),  264.172_052_358_148,  q{1m3 in gallon US via volume} );
iz( $m->pk(),      113.510_373_033_607,  q{1m3 in peck via volume} );
iz( $m->pt_uk(),   1_759.750_388_428_59, q{1m3 in pint via volume} );
iz( $m->pt_us(),   2_113.376_418_865_19, q{1m3 in liquid pint via volume} );
iz( $m->tbsp(),    67_628.045_403_457_3, q{1m3 in tablespoon via volume} );
iz( $m->tsp(),     202_884.136_211_058,  q{1m3 in teaspoon via volume} );

# https://en.wikipedia.org/wiki/Barrel_(unit)
$m = Class::Measure::Scientific::FX_992vb->volume( .1, 'm3' );
Test::More::is( sprintf( q{%.0f}, $m->gal_uk() ), 22, q{100L in UK gallon} );
Test::More::is( sprintf( q{%.0f}, $m->gal_us() ), 26, q{100L in US gallon} );
$m = Class::Measure::Scientific::FX_992vb->volume( .2, 'm3' );
Test::More::is( sprintf( q{%.0f}, $m->gal_uk() ), 44, q{200L in UK gallon} );
Test::More::is( sprintf( q{%.0f}, $m->gal_us() ), 53, q{200L in US gallon} );
$m = Class::Measure::Scientific::FX_992vb->volume( 1, 'dry_barrel' );
Test::More::is( sprintf( q{%.4f}, $m->m3() ), 0.1156, q{1 dry barrel in m3} );
Test::More::is( sprintf( q{%.2f}, $m->bu() ),
    3.28, q{1 dry barrel in US bushels} );
$m = Class::Measure::Scientific::FX_992vb->volume( 0.0955, 'm3' );
Test::More::is( sprintf( q{%.2f}, $m->bu() ), 2.71, q{95.5L in US bushels} );
$m = Class::Measure::Scientific::FX_992vb->volume( .142, 'm3' );
Test::More::is( sprintf( q{%d}, $m->gal_us() ), 37, q{0.142m3 in US gallon} );
$m = Class::Measure::Scientific::FX_992vb->volume( 36, 'gal_uk' );
Test::More::is( sprintf( q{%.0f}, $m->gal_us() ),
    43, q{36 UK gallon in US gallon} );
Test::More::is( sprintf( q{%.3f}, $m->m3() ), 0.164, q{36 UK gallon in m3} );
$m = Class::Measure::Scientific::FX_992vb->volume( 31.5, 'gal_us' );
Test::More::is( sprintf( q{%.0f}, $m->gal_uk() ),
    26, q{31.5 US gallon in UK gallon} );
Test::More::is( sprintf( q{%.3f}, $m->m3() ), 0.119, q{31.5 US gallon in m3} );
$m = Class::Measure::Scientific::FX_992vb->volume( 31, 'gal_us' );
Test::More::is( sprintf( q{%.0f}, $m->gal_uk() ),
    26, q{31 US gallon in UK gallon} );
Test::More::is( sprintf( q{%.3f}, $m->m3() ), 0.117, q{31 US gallon in m3} );
$m = Class::Measure::Scientific::FX_992vb->volume( 1, 'bbl' );
Test::More::is( sprintf( q{%.0f}, $m->gal_us() ), 42,
    q{1 barrel in US gallon} );
Test::More::is( sprintf( q{%.3f}, $m->m3() ), 0.159, q{1 barrel in m3} );
Test::More::is( sprintf( q{%.0f}, $m->gal_uk() ), 35,
    q{1 barrel in UK gallon} );

## no critic (RequireInterpolationOfMetachars)
my $msg = q{Author test. Install Test::NoWarnings and set }
  . q{$ENV{AUTHOR_TESTING} to a true value to run.};
SKIP: {
    if ( !$test_warnings ) {
        Test::More::skip $msg, 1;
    }
}
$test_warnings && Test::NoWarnings::had_no_warnings();
