# -*- cperl; cperl-indent-level: 4 -*-
## no critic (RequireExplicitPackage RequireEndWithOne ProhibitMagicNumbers)
use 5.016;
use strict;
use warnings;
use utf8;

use Test::More;
my $test_warnings = $ENV{'AUTHOR_TESTING'} && eval { require Test::NoWarnings };

use version;

our $VERSION = v0.0.7;
Test::More::plan 'tests' => 17;

sub iz {
    my ( $got, $exp, $msg ) = @_;
    my $FMT = q{%.15g};
    return Test::More::is( sprintf( $FMT, $got ), sprintf( $FMT, $exp ), $msg );
}

require Class::Measure::Scientific::FX_992vb;
my $m = Class::Measure::Scientific::FX_992vb->speed( 1, 'kn' );
iz( $m->kn(),  1,                      q{1kn via speed} );
iz( $m->mps(), 0.514_444_444_444_444,  q{1kn in mps via speed} );
iz( $m->mph(), 1.150_779_448_023_54,   q{1kn in mph via speed} );
iz( $m->c(), 1.716_001_956_408_27e-09, q{1kn in c (speed of light) via speed} );
$m = Class::Measure::Scientific::FX_992vb->speed( 1, 'mps' );
iz( $m->mps(), 1,                   q{1mps via speed} );
iz( $m->kn(),  1.943_844_492_440_6, q{1mps in kn via speed} );
iz( $m->mph(), 2.236_936_292_054_4, q{1mps in mph via speed} );
iz( $m->c(), 3.335_640_951_981_52e-09,
    q{1mps in c (speed of light) via speed} );
$m = Class::Measure::Scientific::FX_992vb->speed( 1, 'mph' );
iz( $m->mph(), 1,                     q{1mph via speed} );
iz( $m->kn(),  0.868_976_241_900_648, q{1mph in kn via speed} );
iz( $m->mps(), 0.447_04,              q{1mph in mps via speed} );
iz( $m->c(), 1.491_164_931_173_82e-09,
    q{1mph in c (speed of light) via speed} );
$m = Class::Measure::Scientific::FX_992vb->speed( 1, 'c' );
iz( $m->c(),   1,                   q{1c via speed} );
iz( $m->kn(),  582_749_918.358_531, q{1c in kn via speed} );
iz( $m->mps(), 299_792_458,         q{1c in mps via speed} );
iz( $m->mph(), 670_616_629.384_395, q{1c mph via speed} );

## no critic (RequireInterpolationOfMetachars)
my $msg = q{Author test. Install Test::NoWarnings and set }
  . q{$ENV{AUTHOR_TESTING} to a true value to run.};
SKIP: {
    if ( !$test_warnings ) {
        Test::More::skip $msg, 1;
    }
}
$test_warnings && Test::NoWarnings::had_no_warnings();
