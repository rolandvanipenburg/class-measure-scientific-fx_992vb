# -*- cperl; cperl-indent-level: 4 -*-
## no critic (RequireExplicitPackage RequireEndWithOne ProhibitMagicNumbers)
use 5.016;
use strict;
use warnings;
use utf8;

use Test::More;
my $test_warnings = $ENV{'AUTHOR_TESTING'} && eval { require Test::NoWarnings };

use version;

our $VERSION = v0.0.7;
Test::More::plan 'tests' => 20;

sub iz {
    my ( $got, $exp, $msg ) = @_;

    # Length unit conversion can lead to repeating digits that are rounded
    # differently based on the use of the -Duselongdouble perl configuration
    # option so we limit the precision of the test to 13 significants:
    my $FMT = q{%.13g};
    return Test::More::is( sprintf( $FMT, $got ), sprintf( $FMT, $exp ), $msg );
}

require Class::Measure::Scientific::FX_992vb;
my $m = Class::Measure::Scientific::FX_992vb->length( 1, 'm' );
iz( $m->m(),  1,                        q{1m via length} );
iz( $m->AU(), 6.684_587_153_547_04e-12, q{1m in atronomical unit via length} );
iz( $m->ly(), 1.057_023_232_313_62e-16, q{1m in lightyear via length} );
iz( $m->nmi(),   0.000_539_956_803_455_724, q{1m in nautical mile via length} );
iz( $m->pc(),    3.240_779_301_513_35e-17,  q{1m in parsec via length} );
iz( $m->chain(), 0.049_709_595_959_596,     q{1m in chain via length} );
iz( $m->fathom(), 0.546_805_555_555_556,    q{1m in fathom via length} );
iz( $m->ft(),     3.280_839_895_013_12,     q{1m in feet via length} );
iz( $m->ft_us(),  3.280_833_333_333_33, q{1m in US surveyors feet via length} );
iz( $m->in(),     39.370_078_740_157_5, q{1m in inch via length} );
iz( $m->mil(),    39_370.078_740_157_5, q{1m in mil via length} );
iz( $m->mile(),   0.000_621_371_192_237_334, q{1m in miles via length} );
iz( $m->sm(), 0.000_621_369_949_494_949, q{1m in US statute miles via length} );
iz( $m->yd(), 1.093_613_298_337_71,      q{1m in yard via length} );
iz( $m->yd_us(), 1.093_611_111_111_11,   q{1m in US yard via length} );
iz( $m->lambdacn(), 757_810_621_458_514,
    q{1m in Compton wavelength n via length} );
iz( $m->lambdacp(), 756_767_449_676_289,
    q{1m in Compton wavelength p via length} );
iz( $m->a0(), 18_897_266_635.103_2, q{1m in Bohr radius via length} );
iz( $m->re(), 354_869_411_605_223,  q{1m in electron radius via length} );

## no critic (RequireInterpolationOfMetachars)
my $msg = q{Author test. Install Test::NoWarnings and set }
  . q{$ENV{AUTHOR_TESTING} to a true value to run.};
SKIP: {
    if ( !$test_warnings ) {
        Test::More::skip $msg, 1;
    }
}
$test_warnings && Test::NoWarnings::had_no_warnings();
