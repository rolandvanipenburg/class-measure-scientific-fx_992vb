# -*- cperl; cperl-indent-level: 4 -*-
## no critic (RequireExplicitPackage RequireEndWithOne)
use 5.016;
use strict;
use warnings;
use utf8;
use Readonly;

use Test::More;
my $test_warnings = $ENV{'AUTHOR_TESTING'} && eval { require Test::NoWarnings };

our $VERSION = v0.0.7;

BEGIN {
## no critic (RequireExplicitInclusion)
    @MAIN::METHODS =
      qw(unit value set_value reg_units units reg_aliases reg_convs);
## no critic (RequireExplicitInclusion ProhibitCallsToUnexportedSubs)
    Readonly::Scalar my $BASE_TESTS => 4;
    Test::More::plan 'tests' => ( $BASE_TESTS + @MAIN::METHODS ) + 1;
    Test::More::ok(1);
    Test::More::use_ok('Class::Measure::Scientific::FX_992vb');
}
Test::More::diag(
    q{Testing Class::Measure::Scientific::FX_992vb }
## no critic (ProhibitCallsToUnexportedSubs RequireExplicitInclusion)
      . $Class::Measure::Scientific::FX_992vb::VERSION,
);
my $fx =
  Test::More::new_ok( 'Class::Measure::Scientific::FX_992vb' => [ 1, q{m} ] );

## no critic (RequireExplicitInclusion)
@Class::Measure::Scientific::FX_992vb::Sub::ISA =
  qw(Class::Measure::Scientific::FX_992vb);
my $fx_sub =
  Test::More::new_ok(
    'Class::Measure::Scientific::FX_992vb::Sub' => [ 1, q{m} ] );

## no critic (RequireExplicitInclusion)
foreach my $method (@MAIN::METHODS) {
    Test::More::can_ok( 'Class::Measure::Scientific::FX_992vb', $method );
}

## no critic (RequireInterpolationOfMetachars)
my $msg = q{Author test. Install Test::NoWarnings and set }
  . q{$ENV{AUTHOR_TESTING} to a true value to run.};
SKIP: {
    if ( !$test_warnings ) {
        Test::More::skip $msg, 1;
    }
}
$test_warnings && Test::NoWarnings::had_no_warnings();
