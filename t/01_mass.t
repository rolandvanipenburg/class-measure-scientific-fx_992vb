# -*- cperl; cperl-indent-level: 4 -*-
## no critic (RequireExplicitPackage RequireEndWithOne ProhibitMagicNumbers)
use 5.016;
use strict;
use warnings;
use utf8;

use Test::More;
my $test_warnings = $ENV{'AUTHOR_TESTING'} && eval { require Test::NoWarnings };

use version;

our $VERSION = v0.0.7;
Test::More::plan 'tests' => 18;

sub iz {
    my ( $got, $exp, $msg ) = @_;
    my $FMT = q{%.15g};
    return Test::More::is( sprintf( $FMT, $got ), sprintf( $FMT, $exp ), $msg );
}

require Class::Measure::Scientific::FX_992vb;
my $m = Class::Measure::Scientific::FX_992vb->mass( 1, 'kg' );
iz( $m->kg(), 1,     q{1 kg via mass} );
iz( $m->ct(), 5_000, q{1 kg in metric carat via mass} );
iz( $m->cwt_uk(), 0.019_684_130_552_221_2,
    q{1 kg in UK hundred weight via mass} );
iz( $m->cwt_us(), 0.022_046_226_218_487_8,
    q{1 kg in US hundred weight via mass} );
iz( $m->gr(),     15_432.358_352_941_4,      q{1 kg in grain via mass} );
iz( $m->lb(),     2.204_622_621_848_78,      q{1 kg in pound via mass} );
iz( $m->lbt(),    2.679_228_880_719,         q{1 kg in troy pound via mass} );
iz( $m->oz(),     35.273_961_955_801_7,      q{1 kg in ounce via mass} );
iz( $m->ozt(),    32.150_746_568_628,        q{1 kg in troy ounce via mass} );
iz( $m->slug(),   0.068_521_765_856_821_6,   q{1 kg in slug via mass} );
iz( $m->ton_uk(), 0.000_984_206_527_611_061, q{1 kg in long ton via mass} );
iz( $m->ton_us(), 0.001_102_311_310_924_39,  q{1 kg in short ton via mass} );
iz( $m->m1h(), 5.975_288_358_453_25e+26,
    q{1 kg in mass of hydrogen atom via mass} );
iz( $m->me(), 1.097_750_993_629_31e+30, q{1 kg in mass of electron via mass} );
iz( $m->mn(), 5.970_312_145_232_86e+26, q{1 kg in mass of neutron via mass} );
iz( $m->mp(), 5.978_542_413_423_98e+26, q{1 kg in mass of proton via mass} );
iz( $m->u(),  6.022_044_899_764_57e+26, q{1 kg in atomic mass unit via mass} );

## no critic (RequireInterpolationOfMetachars)
my $msg = q{Author test. Install Test::NoWarnings and set }
  . q{$ENV{AUTHOR_TESTING} to a true value to run.};
SKIP: {
    if ( !$test_warnings ) {
        Test::More::skip $msg, 1;
    }
}
$test_warnings && Test::NoWarnings::had_no_warnings();
