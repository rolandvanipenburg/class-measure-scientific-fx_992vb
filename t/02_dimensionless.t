# -*- cperl; cperl-indent-level: 4 -*-
## no critic (RequireExplicitPackage RequireEndWithOne ProhibitMagicNumbers)
use 5.016;
use strict;
use warnings;
use utf8;

use Test::More;
my $test_warnings = $ENV{'AUTHOR_TESTING'} && eval { require Test::NoWarnings };

use version;

our $VERSION = v0.0.7;
Test::More::plan 'tests' => 7;

require Class::Measure::Scientific::FX_992vb;
## no critic (ProhibitCallsToUnexportedSubs)
Test::More::is( Class::Measure::Scientific::FX_992vb::dB(),
    0.115_129_254_65, q{Decibel difference level} );
Test::More::is( Class::Measure::Scientific::FX_992vb::Np(),
    8.685_889_638_07, q{Neper difference level} );
Test::More::is( Class::Measure::Scientific::FX_992vb::a(),
    7.297_350_6e-3, q{Fine-structure constant} );
Test::More::is( Class::Measure::Scientific::FX_992vb::zC(),
    273.15, q{zero Celsius in Kelvin} );
Test::More::is( Class::Measure::Scientific::FX_992vb::zF(),
    -160 / 9, q{zero Fahrenheit in Kelvin} );
Test::More::is( Class::Measure::Scientific::FX_992vb::FK(),
    5 / 9, q{degree Fahrenheit in Kelvin} );

## no critic (RequireInterpolationOfMetachars)
my $msg = q{Author test. Install Test::NoWarnings and set }
  . q{$ENV{AUTHOR_TESTING} to a true value to run.};
SKIP: {
    if ( !$test_warnings ) {
        Test::More::skip $msg, 1;
    }
}
$test_warnings && Test::NoWarnings::had_no_warnings();
