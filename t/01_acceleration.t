# -*- cperl; cperl-indent-level: 4 -*-
## no critic (RequireExplicitPackage RequireEndWithOne ProhibitMagicNumbers)
use 5.016;
use strict;
use warnings;
use utf8;

use Test::More;
my $test_warnings = $ENV{'AUTHOR_TESTING'} && eval { require Test::NoWarnings };

use version;

our $VERSION = v0.0.7;
Test::More::plan 'tests' => 5;

sub iz {
    my ( $got, $exp, $msg ) = @_;
    my $FMT = q{%.15g};
    return Test::More::is( sprintf( $FMT, $got ), sprintf( $FMT, $exp ), $msg );
}

require Class::Measure::Scientific::FX_992vb;
my $m = Class::Measure::Scientific::FX_992vb->acceleration( 1, 'g' );
iz( $m->g(),    1,        q{1g via acceleration} );
iz( $m->mps2(), 9.806_65, q{1g in mps2 via acceleration} );
$m = Class::Measure::Scientific::FX_992vb->acceleration( 1, 'mps2' );
iz( $m->mps2(), 1,                     q{1mps2 via acceleration} );
iz( $m->g(),    0.101_971_621_297_793, q{1mps2 in g via acceleration} );

## no critic (RequireInterpolationOfMetachars)
my $msg = q{Author test. Install Test::NoWarnings and set }
  . q{$ENV{AUTHOR_TESTING} to a true value to run.};
SKIP: {
    if ( !$test_warnings ) {
        Test::More::skip $msg, 1;
    }
}
$test_warnings && Test::NoWarnings::had_no_warnings();
