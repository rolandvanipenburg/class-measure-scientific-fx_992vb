# -*- cperl; cperl-indent-level: 4 -*-
## no critic (RequireExplicitPackage RequireEndWithOne ProhibitMagicNumbers)
use 5.016;
use strict;
use warnings;
use utf8;

use Test::More;
my $test_warnings = $ENV{'AUTHOR_TESTING'} && eval { require Test::NoWarnings };

use version;

our $VERSION = v0.0.7;
Test::More::plan 'tests' => 5;

sub iz {
    my ( $got, $exp, $msg ) = @_;
    my $FMT = q{%.15g};
    return Test::More::is( sprintf( $FMT, $got ), sprintf( $FMT, $exp ), $msg );
}

require Class::Measure::Scientific::FX_992vb;
my $m = Class::Measure::Scientific::FX_992vb->duration( 1, 's' );
iz( $m->s(),    1,                        q{1s via duration} );
iz( $m->year(), 3.168_876_461_541_28e-08, q{1s in years via duration} );
$m = Class::Measure::Scientific::FX_992vb->duration( 1, 'year' );
iz( $m->year(), 1,          q{1 year via duration} );
iz( $m->s(),    31_556_926, q{1 year in seconds via duration} );

## no critic (RequireInterpolationOfMetachars)
my $msg = q{Author test. Install Test::NoWarnings and set }
  . q{$ENV{AUTHOR_TESTING} to a true value to run.};
SKIP: {
    if ( !$test_warnings ) {
        Test::More::skip $msg, 1;
    }
}
$test_warnings && Test::NoWarnings::had_no_warnings();
