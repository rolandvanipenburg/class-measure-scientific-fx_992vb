# -*- cperl; cperl-indent-level: 4 -*-
## no critic (RequireExplicitPackage RequireEndWithOne ProhibitMagicNumbers)
use 5.016;
use strict;
use warnings;
use utf8;

use Test::More;
my $test_warnings = $ENV{'AUTHOR_TESTING'} && eval { require Test::NoWarnings };

use version;

our $VERSION = v0.0.7;
Test::More::plan 'tests' => 9;

sub iz {
    my ( $got, $exp, $msg ) = @_;
    my $FMT = q{%.15g};
    return Test::More::is( sprintf( $FMT, $got ), sprintf( $FMT, $exp ), $msg );
}

require Class::Measure::Scientific::FX_992vb;
my $m = Class::Measure::Scientific::FX_992vb->energy( 1, 'J' );
iz( $m->J(),     1,                         q{1 Joule via energy} );
iz( $m->BTU(),   0.000_947_817_120_313_317, q{1 Joule in BTU} );
iz( $m->ftlbf(), 0.737_562_149_278_027,     q{1 Joule in foot pound-force} );
iz( $m->hp(),    0.001_341_022_089_595_51,  q{1 Joule in horsepower} );
iz( $m->cal15(), 0.238_920_081_232_828,     q{1 Joule in 15 degree calorie} );
iz( $m->calit(), 0.238_845_896_627_496,     q{1 Joule in I.T. calorie} );
iz( $m->calth(), 0.239_005_736_137_667, q{1 Joule in thermo chemical calorie} );
iz( $m->therm(), 9.478_171_203_133_17e-09, q{1 Joule in therm} );

## no critic (RequireInterpolationOfMetachars)
my $msg = q{Author test. Install Test::NoWarnings and set }
  . q{$ENV{AUTHOR_TESTING} to a true value to run.};
SKIP: {
    if ( !$test_warnings ) {
        Test::More::skip $msg, 1;
    }
}
$test_warnings && Test::NoWarnings::had_no_warnings();
