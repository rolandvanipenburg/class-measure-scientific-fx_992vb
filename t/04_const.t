# -*- cperl; cperl-indent-level: 4 -*-
## no critic (RequireExplicitPackage RequireEndWithOne ProhibitMagicNumbers)
use 5.016;
use strict;
use warnings;
use utf8;

use Test::More;
my $test_warnings = $ENV{'AUTHOR_TESTING'} && eval { require Test::NoWarnings };

use version;

our $VERSION = v0.0.7;
Test::More::plan 'tests' => 7;

require Class::Measure::Scientific::FX_992vb;
my $c;
## no critic (ProhibitCallsToUnexportedSubs)
$c = Class::Measure::Scientific::FX_992vb::CONST(1);
Test::More::is( $c, 0.017_453_292_51, q{CONST 1} );
$c = Class::Measure::Scientific::FX_992vb::CONST(127);
Test::More::is( $c, 2.58e-4, q{CONST 127} );
$c = Class::Measure::Scientific::FX_992vb::CONST(128);
Test::More::is( $c, 2.067_850_6e-15, q{CONST 128} );
$c = Class::Measure::Scientific::FX_992vb::CONST(0);
Test::More::is( $c, undef, q{CONST 0} );
$c = Class::Measure::Scientific::FX_992vb::CONST(129);
Test::More::is( $c, undef, q{CONST 129} );
$c = Class::Measure::Scientific::FX_992vb::CONST(999);
Test::More::is( $c, undef, q{CONST 999} );

## no critic (RequireInterpolationOfMetachars)
my $msg = q{Author test. Install Test::NoWarnings and set }
  . q{$ENV{AUTHOR_TESTING} to a true value to run.};
SKIP: {
    if ( !$test_warnings ) {
        Test::More::skip $msg, 1;
    }
}
$test_warnings && Test::NoWarnings::had_no_warnings();
