# -*- cperl; cperl-indent-level: 4 -*-
## no critic (RequireExplicitPackage RequireEndWithOne ProhibitMagicNumbers)
use 5.016;
use strict;
use warnings;
use utf8;

use Test::More;
my $test_warnings = $ENV{'AUTHOR_TESTING'} && eval { require Test::NoWarnings };

use version;

our $VERSION = v0.0.7;
Test::More::plan 'tests' => 6;

require Class::Measure::Scientific::FX_992vb;
my $d = Class::Measure::Scientific::FX_992vb->length( 1, 'nmi' );
my $s = Class::Measure::Scientific::FX_992vb->speed( 1, 'kn' );
Test::More::is( $d->mile(), $s->mph(), q{Knots equal nautical miles per hour} );

$d = Class::Measure::Scientific::FX_992vb->duration( 1, 'year' );
$s = Class::Measure::Scientific::FX_992vb->speed( 1, 'c' );
my $l   = Class::Measure::Scientific::FX_992vb->length( 1, 'ly' );
my $acc = 5;
Test::More::is(
    sprintf( qq{%.${acc}e}, $d->s() * $s->mps() ),
    sprintf( qq{%.${acc}e}, $l->m() ),
    q{Light travels approximately one light-year per tropical year},
);

$d = Class::Measure::Scientific::FX_992vb->length( 1, 'mile' );
my $m =
  Class::Measure::Scientific::FX_992vb->area( ( ( $d->m()**2 ) / 640 ), 'm2' );
my $a = Class::Measure::Scientific::FX_992vb->area( 1, 'acre' );
Test::More::is(
    sprintf( qq{%.${acc}e}, $m->m2() ),
    sprintf( qq{%.${acc}e}, $a->m2() ),
    q{One acre is 1/640 of a square mile},
);

$d = Class::Measure::Scientific::FX_992vb->length( 1, 'ft' );
my $i = Class::Measure::Scientific::FX_992vb->length( 1, 'in' );
my $v =
  Class::Measure::Scientific::FX_992vb->volume( $i->m() * $d->m()**2, 'm3' );
Test::More::is( sprintf( qq{%.${acc}f}, $v->m3() ),
    0.00236, q{Board foot calculated from dimensions} );

$d = Class::Measure::Scientific::FX_992vb->length( 1, 'mile' );
Test::More::is( $d->m() / 3600,
    0.44704, q{One mile per hour in meters per second} );

## no critic (RequireInterpolationOfMetachars)
my $msg = q{Author test. Install Test::NoWarnings and set }
  . q{$ENV{AUTHOR_TESTING} to a true value to run.};
SKIP: {
    if ( !$test_warnings ) {
        Test::More::skip $msg, 1;
    }
}
$test_warnings && Test::NoWarnings::had_no_warnings();
